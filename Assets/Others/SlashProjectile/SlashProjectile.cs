using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashProjectile : MonoBehaviour
{
    void Update()
    {
        Destroy(gameObject, 0.15f);
        transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale * 2f, Time.deltaTime * 10);
        transform.Translate(Vector3.forward * 20 * Time.deltaTime);
    }
}
