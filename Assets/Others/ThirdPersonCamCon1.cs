﻿namespace PlayerCameraFollow
{
    using System;
    using UnityEngine;
    using System.Collections.Generic;

    public class ThirdPersonCamCon1 : MonoBehaviour
    {

        [SerializeField] private CameraMotionSettings motion = new CameraMotionSettings();
        [SerializeField] Transform target;
        [SerializeField] CursorLockMode lockMode;
        private CameraSettings mySettings;
        private Transform FollowTarget;
        private Transform CameraTransform;

        private List<float> rootListX = new List<float>();
        private List<float> rootListY = new List<float>();

        private float SmootX
        {
            get
            {
                float returned = 0f;
                rootListX.ForEach(x => returned += x);
                return returned /= rootListX.Count;
            }
            set
            {
                rootListX.Add(value);
                if (rootListX.Count > mySettings.SmootPower) rootListX.RemoveAt(0);
            }
        }
        private float SmootY
        {
            get
            {
                float returned = 0f;
                rootListY.ForEach(x => returned += x);
                return returned /= rootListY.Count;
            }
            set
            {
                rootListY.Add(value);
                if (rootListY.Count > mySettings.SmootPower) rootListY.RemoveAt(0);
            }
        }

        private float RootX;
        private float RootY;

        private int InvY = 1;
        private int InvX = 1;

        private List<Vector3> movementList = new List<Vector3>();
        private Vector3 MovementFrame
        {
            get
            {
                Vector3 pos = Vector3.zero;
                movementList.ForEach(x => pos += x);
                pos /= mySettings.SmootPowerFollow;
                return pos;
            }
            set { movementList.Add(value); if (movementList.Count > mySettings.SmootPowerFollow) movementList.RemoveAt(0); }
        }

        public float PlayerEulerY { get { return SmootX; } }
        public Vector3 CameraDirr { get { return CameraTransform.rotation * Vector3.forward; } }
        public Vector3 CameraEuler { get { return new Vector3(SmootY, ClampAngle(SmootX, mySettings.rangeX), 0f); } }
        public Quaternion CameraQuaternion { get { return Quaternion.Euler(SmootY, ClampAngle(SmootX, mySettings.rangeX), 0f); } }

        public void InitCameraFollow(Transform _target)
        {
            motion = new CameraMotionSettings();
            motion.Rings = new List<Ring>();
            motion.Rings.Add(new Ring(1f, 1f));
            motion.Rings.Add(new Ring(0f, 1f));
            motion.Rings.Add(new Ring(-1f, 0.4f));

            CameraSettings sett;

            sett.InverseY = false;
            sett.InverseX = true;
            sett.sensX = 3.3f;
            sett.sensY = 2.8f;
            sett.SmootPower = 17;
            sett.SmootPowerFollow = 15;

            sett.rangeX = new RootRange(-360f, 360f);
            sett.rangeY = new RootRange(-65f, 80f);

            motion.FollowDistance = 7;//3.25f;
            motion.CreateDistanceCruve(sett);

            mySettings = sett;

            InvY = Convert.ToInt32(mySettings.InverseY); if (InvY == 0) InvY = -1;
            InvX = Convert.ToInt32(mySettings.InverseX); if (InvY == 0) InvY = -1;

            FollowTarget = _target;
        }

        private void Start()
        {
            Cursor.lockState = lockMode;
            InitCameraFollow(target);
        }
        private void Update()
        {
            if (!FollowTarget) return;
            #region PlayerInput
            SmootY = RootY = Clamp(RootY += Input.GetAxis("Mouse Y") /** (SettingsGameContol.InvertMouseSettings ? -1 : 1)*/ * (mySettings.sensY * InvY), mySettings.rangeY);
            float bakedY = SmootY;
            SmootX = RootX += Input.GetAxis("Mouse X") /** (SettingsGameContol.InvertMouseSettings ? -1 : 1)*/ * (mySettings.sensX * InvX);
            CameraTransform.rotation = Quaternion.Euler(bakedY, ClampAngle(SmootX, mySettings.rangeX), 0f);
            //MovementFrame = FollowTarget.position;
            CameraTransform.position = CameraTransform.rotation * new Vector3(0, 0, -motion.FollowDistance/* * motion.GetDistanceFrame(bakedY)*/) + FollowTarget.position;
            #endregion
        }

        private float Clamp(float _angle, RootRange _range)
        {
            if (_angle > 360f) _angle -= 360f;
            if (_angle < -360f) _angle += 360f;

            if (_angle < _range.MaxRange && _angle > _range.MinRange) return _angle;
            if (_angle > _range.MaxRange) return _range.MaxRange;
            if (_angle < _range.MinRange) return _range.MinRange;
            return Mathf.Clamp(_angle, _range.MinRange, _range.MaxRange);
        }
        private float ClampAngle(float angle, RootRange _range)
        {
            angle = angle % 360;
            if ((angle >= -360F) && (angle <= 360F))
            {
                if (angle < -360F) angle += 360F;
                if (angle > 360F) angle -= 360F;
            }
            return Mathf.Clamp(angle, _range.MinRange, _range.MaxRange);
        }

        #region Test's
        private void Awake()
        {
            CameraTransform = Camera.main.transform;
        }
        #endregion

    }

    [Serializable]
    public struct CameraSettings
    {
        public bool InverseY;
        public bool InverseX;
        public float sensX;
        public float sensY;
        public int SmootPower;
        public int SmootPowerFollow;

        public RootRange rangeX;
        public RootRange rangeY;
    }
    [Serializable]
    public struct RootRange
    {
        public float MinRange;
        public float MaxRange;

        public RootRange(float _min, float _max)
        {
            MinRange = _min; MaxRange = _max;
        }
        public static RootRange operator *(RootRange _a, float _b)
        {
            _a.MinRange *= _b; _a.MaxRange *= _b;
            return _a;
        }
    }
    [Serializable]
    public struct CameraMotionSettings
    {
        public List<Ring> Rings;
        public float FollowDistance;
        private AnimationCurve xDistCruve;

        public float GetDistanceFrame(float _time)
        {
            return xDistCruve.Evaluate(_time);
        }
        public void CreateDistanceCruve(CameraSettings _settings)
        {
            Keyframe[] keys = new Keyframe[Rings.Count];
            Rings.Sort((x, y) => x.RingOffset.CompareTo(y.RingOffset));
            float Time = _settings.rangeY.MinRange;
            float timeOffset = (Mathf.Abs(_settings.rangeY.MinRange) + Mathf.Abs(_settings.rangeY.MaxRange)) / (Rings.Count - 1);
            for (int i = 0; i < Rings.Count; i++)
            {
                keys[i].inTangent = Rings[i].InAngle;
                keys[i].outTangent = Rings[i].OutAgle;

                keys[i].time = Time + timeOffset * i;
                keys[i].value = Rings[i].RingRadius;
            }
            xDistCruve = new AnimationCurve(keys);
        }
    }
    [Serializable]
    public struct Ring
    {
        public float RingOffset;
        public float RingRadius;
        public Transform LookAtTarget;

        public float InAngle;
        public float OutAgle;
        public Ring(float offset, float radius)
        {
            RingRadius = radius;
            RingOffset = offset;
            LookAtTarget = null;
            InAngle = 0;
            OutAgle = 0;
        }
    }
}
