
// When creating shaders for Universal Render Pipeline you can you the ShaderGraph which is super AWESOME!
// However, if you want to author shaders in shading language you can use this teamplate as a base.
// Please note, this shader does not necessarily match perfomance of the built-in URP Lit shader.
// This shader works with URP 7.1.x and above
Shader "Universal Render Pipeline/Custom/Clouds Unlit Vertex Color"
{
	Properties
	{
		_TopTexture0("Top Texture 0", 2D) = "white" {}
		_TessValue("Max Tessellation", Range(1, 32)) = 21.6
		_TessMin("Tess Min Distance", Float) = 0.7
		_TessMax("Tess Max Distance", Float) = 10.94
		_NoiseScaleA("NoiseScale A", Vector) = (1,1,1,0)
		_3dNoiseSizeA("3dNoise Size A", Float) = 0
		_SpeedA("Speed A", Float) = 0
		_DirectionA("Direction A", Vector) = (1,0,0,0)
		_NoiseStrengthA("Noise Strength A", Range(0 , 1)) = 0
		_3dNoiseSizeB("3dNoise Size B", Float) = 0
		_NoiseScaleB("NoiseScale B", Vector) = (1,1,1,0)
		_SpeedB("Speed B", Float) = 0
		_DirectionB("Direction B", Vector) = (1,0,0,0)
		_NoiseStrengthB("Noise Strength B", Range(0 , 1)) = 0
		_NoiseScaleC("NoiseScale C", Vector) = (1,1,1,0)
		_3dNoiseSizeC("3dNoise Size C", Float) = 0
		_SpeedC("SpeedC", Float) = 0
		_DirectionC("DirectionC", Vector) = (1,0,0,0)
		_NoiseStrengthC("Noise Strength C", Range(0 , 1)) = 0
		_textureDetail("textureDetail", Range(0 , 1)) = 0
		_TextureColor("Texture Color", Color) = (0,0,0,0)
		_Tiling("Tiling", Vector) = (0,0,0,0)
		_Fallof("Fallof", Float) = 0
		_VertexColorMult("Vertex Color Mult", Float) = 1
		_RimColor("Rim Color", Color) = (0,0,0,0)
		_FresnelBSP("FresnelBSP", Vector) = (0,0,0,0)
		[HideInInspector] __dirty("", Int) = 1
	}
		SubShader
		{
			// With SRP we introduce a new "RenderPipeline" tag in Subshader. This allows to create shaders
		// that can match multiple render pipelines. If a RenderPipeline tag is not set it will match
		  // any render pipeline. In case you want your subshader to only run in LWRP set the tag to
		  // "UniversalRenderPipeline"
			Tags{ "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline" "Queue" = "Geometry+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
			LOD 300
			Cull Back
			CGINCLUDE
			#include "UnityShaderVariables.cginc"
			#include "Tessellation.cginc"
			#include "UnityPBSLighting.cginc"
			#include "Lighting.cginc"
			#pragma target 4.6
			#define ASE_TEXTURE_PARAMS(textureName) textureName

			#ifdef UNITY_PASS_SHADOWCASTER
				#undef INTERNAL_DATA
				#undef WorldReflectionVector
				#undef WorldNormalVector
				#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
				#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
				#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
			#endif
			struct Input
			{
				float3 worldPos;
				float4 vertexColor : COLOR;
				float3 worldNormal;
				INTERNAL_DATA
			};

			uniform float _NoiseStrengthA;
			uniform float3 _DirectionA;
			uniform float _SpeedA;
			uniform float _3dNoiseSizeA;
			uniform float3 _NoiseScaleA;
			uniform float3 _DirectionB;
			uniform float _SpeedB;
			uniform float _3dNoiseSizeB;
			uniform float3 _NoiseScaleB;
			uniform float _NoiseStrengthB;
			uniform float3 _DirectionC;
			uniform float _SpeedC;
			uniform float _3dNoiseSizeC;
			uniform float3 _NoiseScaleC;
			uniform float _NoiseStrengthC;
			uniform float _VertexColorMult;
			uniform float4 _TextureColor;
			uniform sampler2D _TopTexture0;
			uniform float2 _Tiling;
			uniform float _Fallof;
			uniform float _textureDetail;
			uniform float3 _FresnelBSP;
			uniform float4 _RimColor;
			uniform float _TessValue;
			uniform float _TessMin;
			uniform float _TessMax;


			float3 mod3D289(float3 x) { return x - floor(x / 289.0) * 289.0; }
			float4 mod3D289(float4 x) { return x - floor(x / 289.0) * 289.0; }
			float4 permute(float4 x) { return mod3D289((x * 34.0 + 1.0) * x); }
			float4 taylorInvSqrt(float4 r) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise(float3 v)
			{
				const float2 C = float2(1.0 / 6.0, 1.0 / 3.0);
				float3 i = floor(v + dot(v, C.yyy));
				float3 x0 = v - i + dot(i, C.xxx);
				float3 g = step(x0.yzx, x0.xyz);
				float3 l = 1.0 - g;
				float3 i1 = min(g.xyz, l.zxy);
				float3 i2 = max(g.xyz, l.zxy);
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289(i);
				float4 p = permute(permute(permute(i.z + float4(0.0, i1.z, i2.z, 1.0)) + i.y + float4(0.0, i1.y, i2.y, 1.0)) + i.x + float4(0.0, i1.x, i2.x, 1.0));
				float4 j = p - 49.0 * floor(p / 49.0);  // mod(p,7*7)
				float4 x_ = floor(j / 7.0);
				float4 y_ = floor(j - 7.0 * x_);  // mod(j,N)
				float4 x = (x_ * 2.0 + 0.5) / 7.0 - 1.0;
				float4 y = (y_ * 2.0 + 0.5) / 7.0 - 1.0;
				float4 h = 1.0 - abs(x) - abs(y);
				float4 b0 = float4(x.xy, y.xy);
				float4 b1 = float4(x.zw, y.zw);
				float4 s0 = floor(b0) * 2.0 + 1.0;
				float4 s1 = floor(b1) * 2.0 + 1.0;
				float4 sh = -step(h, 0.0);
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3(a0.xy, h.x);
				float3 g1 = float3(a0.zw, h.y);
				float3 g2 = float3(a1.xy, h.z);
				float3 g3 = float3(a1.zw, h.w);
				float4 norm = taylorInvSqrt(float4(dot(g0, g0), dot(g1, g1), dot(g2, g2), dot(g3, g3)));
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max(0.6 - float4(dot(x0, x0), dot(x1, x1), dot(x2, x2), dot(x3, x3)), 0.0);
				m = m * m;
				m = m * m;
				float4 px = float4(dot(x0, g0), dot(x1, g1), dot(x2, g2), dot(x3, g3));
				return 42.0 * dot(m, px);
			}
			inline float4 TriplanarSamplingSF(sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index)
			{
				float3 projNormal = (pow(abs(worldNormal), falloff));
				projNormal /= (projNormal.x + projNormal.y + projNormal.z) + 0.00001;
				float3 nsign = sign(worldNormal);
				half4 xNorm; half4 yNorm; half4 zNorm;
				xNorm = (tex2D(ASE_TEXTURE_PARAMS(topTexMap), tiling * worldPos.zy * float2(nsign.x, 1.0)));
				yNorm = (tex2D(ASE_TEXTURE_PARAMS(topTexMap), tiling * worldPos.xz * float2(nsign.y, 1.0)));
				zNorm = (tex2D(ASE_TEXTURE_PARAMS(topTexMap), tiling * worldPos.xy * float2(-nsign.z, 1.0)));
				return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
			}
			float3 mod2D289(float3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
			float2 mod2D289(float2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
			float3 permute(float3 x) { return mod2D289(((x * 34.0) + 1.0) * x); }
			float snoise(float2 v)
			{
				const float4 C = float4(0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439);
				float2 i = floor(v + dot(v, C.yy));
				float2 x0 = v - i + dot(i, C.xx);
				float2 i1;
				i1 = (x0.x > x0.y) ? float2(1.0, 0.0) : float2(0.0, 1.0);
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289(i);
				float3 p = permute(permute(i.y + float3(0.0, i1.y, 1.0)) + i.x + float3(0.0, i1.x, 1.0));
				float3 m = max(0.5 - float3(dot(x0, x0), dot(x12.xy, x12.xy), dot(x12.zw, x12.zw)), 0.0);
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac(p * C.www) - 1.0;
				float3 h = abs(x) - 0.5;
				float3 ox = floor(x + 0.5);
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * (a0 * a0 + h * h);
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot(m, g);
			}
			float4 tessFunction(appdata_full v0, appdata_full v1, appdata_full v2)
			{
				return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue);
			}
			void vertexDataFunc(inout appdata_full v)
			{
				float3 objToWorldDir239 = mul(unity_ObjectToWorld, float4(float3(0,1,0), 0)).xyz;
				float mulTime15 = _Time.y * _SpeedA;
				float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex);
				float simplePerlin3D3 = snoise(((_DirectionA * mulTime15) + (_3dNoiseSizeA * (ase_worldPos * _NoiseScaleA))));
				float temp_output_8_0 = (0.0 + (simplePerlin3D3 - -1.0) * (1.0 - 0.0) / (1.0 - -1.0));
				float3 objToWorldDir213 = mul(unity_ObjectToWorld, float4(float3(0,1,0), 0)).xyz;
				float mulTime75 = _Time.y * _SpeedB;
				float simplePerlin3D78 = snoise(((_DirectionB * mulTime75) + (_3dNoiseSizeB * (ase_worldPos * _NoiseScaleB))));
				float3 objToWorldDir257 = mul(unity_ObjectToWorld, float4(float3(0,1,0), 0)).xyz;
				float mulTime248 = _Time.y * _SpeedC;
				float3 temp_output_252_0 = ((_DirectionC * mulTime248) + (_3dNoiseSizeC * (ase_worldPos * _NoiseScaleC)));
				float simplePerlin3D255 = snoise(temp_output_252_0);
				v.vertex.xyz += ((objToWorldDir239 * _NoiseStrengthA * temp_output_8_0) + (objToWorldDir213 * (0.0 + (simplePerlin3D78 - -1.0) * (1.0 - 0.0) / (1.0 - -1.0)) * _NoiseStrengthB) + (objToWorldDir257 * (0.0 + (simplePerlin3D255 - -1.0) * (1.0 - 0.0) / (1.0 - -1.0)) * _NoiseStrengthC));
			}
			inline half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten)
			{
				return half4 (0, 0, 0, s.Alpha);
			}
			void surf(Input i , inout SurfaceOutput o)
			{
				o.Normal = float3(0,0,1);
				float3 ase_worldPos = i.worldPos;
				float3 ase_worldNormal = WorldNormalVector(i, float3(0, 0, 1));
				float mulTime248 = _Time.y * _SpeedC;
				float3 temp_output_252_0 = ((_DirectionC * mulTime248) + (_3dNoiseSizeC * (ase_worldPos * _NoiseScaleC)));
				float3 NoiseWorldPos364 = temp_output_252_0;
				float4 triplanar194 = TriplanarSamplingSF(_TopTexture0, NoiseWorldPos364, ase_worldNormal, _Fallof, _Tiling, 1.0, 0);
				float mulTime15 = _Time.y * _SpeedA;
				float simplePerlin3D3 = snoise(((_DirectionA * mulTime15) + (_3dNoiseSizeA * (ase_worldPos * _NoiseScaleA))));
				float temp_output_8_0 = (0.0 + (simplePerlin3D3 - -1.0) * (1.0 - 0.0) / (1.0 - -1.0));
				float NoiseA366 = temp_output_8_0;
				float2 appendResult376 = (float2(ase_worldPos.x , ase_worldPos.z));
				float simplePerlin2D374 = snoise((appendResult376 * 0.1));
				float4 lerpResult210 = lerp(saturate((pow(i.vertexColor , 0.454545) * _VertexColorMult)) , _TextureColor , (((1.0 - triplanar194.x) * _textureDetail) * saturate(NoiseA366) * (0.25 + (simplePerlin2D374 - -1.0) * (1.0 - 0.25) / (1.0 - -1.0))));
				float3 ase_worldViewDir = normalize(UnityWorldSpaceViewDir(ase_worldPos));
				float fresnelNdotV346 = dot(ase_worldNormal, ase_worldViewDir);
				float fresnelNode346 = (_FresnelBSP.x + _FresnelBSP.y * pow(1.0 - fresnelNdotV346, _FresnelBSP.z));
				o.Emission = saturate((lerpResult210 + (fresnelNode346 * _RimColor))).rgb;
				o.Alpha = 1;
			}
			ENDCG
			CGPROGRAM
			#pragma surface surf Unlit keepalpha fullforwardshadows vertex:vertexDataFunc tessellate:tessFunction 
			ENDCG
			Pass
			{
				// "Lightmode" tag must be "UniversalForward" or not be defined in order for
				// to render objects.
				Name "ShadowCaster"
				//Tags{ "LightMode" = "ShadowCaster" }
				Tags{ "LightMode" = "UniversalForward" }
				ZWrite On
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 4.6
				#pragma multi_compile_shadowcaster
				#pragma multi_compile UNITY_PASS_SHADOWCASTER
				#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
				#include "HLSLSupport.cginc"
				#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
					#define CAN_SKIP_VPOS
				#endif
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "UnityPBSLighting.cginc"
				/*#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/Shaders/LitInput.hlsl"*/
				struct v2f
				{
					V2F_SHADOW_CASTER;
					float4 tSpace0 : TEXCOORD1;
					float4 tSpace1 : TEXCOORD2;
					float4 tSpace2 : TEXCOORD3;
					half4 color : COLOR0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};
				v2f vert(appdata_full v)
				{
					v2f o;
					UNITY_SETUP_INSTANCE_ID(v);
					UNITY_INITIALIZE_OUTPUT(v2f, o);
					UNITY_TRANSFER_INSTANCE_ID(v, o);
					Input customInputData;
					vertexDataFunc(v);
					float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
					half3 worldNormal = UnityObjectToWorldNormal(v.normal);
					half3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
					half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
					half3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;
					o.tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
					o.tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
					o.tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);
					TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
					o.color = v.color;
					return o;
				}
				half4 frag(v2f IN
				#if !defined( CAN_SKIP_VPOS )
				, UNITY_VPOS_TYPE vpos : VPOS
				#endif
				) : SV_Target
				{
					UNITY_SETUP_INSTANCE_ID(IN);
					Input surfIN;
					UNITY_INITIALIZE_OUTPUT(Input, surfIN);
					float3 worldPos = float3(IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w);
					half3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
					surfIN.worldPos = worldPos;
					surfIN.worldNormal = float3(IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z);
					surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
					surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
					surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
					surfIN.vertexColor = IN.color;
					SurfaceOutput o;
					UNITY_INITIALIZE_OUTPUT(SurfaceOutput, o)
					surf(surfIN, o);
					#if defined( CAN_SKIP_VPOS )
					float2 vpos = IN.pos;
					#endif
					SHADOW_CASTER_FRAGMENT(IN)
				}
				ENDCG
			}
		}
			Fallback "Diffuse"
						CustomEditor "ASEMaterialInspector"
}
