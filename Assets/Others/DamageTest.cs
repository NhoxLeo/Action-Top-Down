using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTest : MonoBehaviour
{
    private MeshRenderer meshRenderer;
    private Material unitMaterial;

    [SerializeField]
    private float destroyDelay = 1;
    [SerializeField]
    private float damageBlinkDecrease = 0.85f;
    private float damageBlinkRatio = 1;
    [SerializeField]
    private Material damageMaterial;
    bool isDamageBlinking = false;

    float damageTime;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        unitMaterial = meshRenderer.materials[meshRenderer.materials.Length - 1];
        unitMaterial.SetFloat("_WhitePiece", UnityEngine.Random.Range(0, 10) > 5f ? 1 : 0);

    }

    void Update()
    {

    }
    void FixedUpdate()
    {
        if (isDamageBlinking)
        {
            damageBlinkRatio = damageBlinkRatio * damageBlinkDecrease;

            if (damageBlinkRatio < 0.01f)
            {
                isDamageBlinking = false;
                damageBlinkRatio = 0;
            }

            unitMaterial.SetFloat("_Damage", damageBlinkRatio);
        }

        damageTime = Mathf.Lerp(damageTime, 0, Time.deltaTime * 5);
        unitMaterial.SetFloat("_DamageEffectTime", damageTime);
    }
    public void TakeDamage(float damageToTake)
    {

        damageBlinkRatio = 1;
        unitMaterial.SetFloat("_Damage", 1);
        isDamageBlinking = true;

        damageTime = 1;
    }
}
