﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
//using EnemyBehaviours.Internal;

public class RigidbodyCharacter : MonoBehaviour
{

    public float Speed = 5f;
    public float RotateSpeed = 5f;
    public float JumpHeight = 2f;
    public float GroundDistance = 0.2f;
    public float DashDistance = 5f;
    public LayerMask Ground;

    private Rigidbody _body;
    private Animator _animator;
    private Vector3 _inputs = Vector3.zero;
    private Quaternion _rotation;
    private Vector3 m_CamForward;
    private bool _isGrounded = true;
    private Transform _groundChecker;

    private float attackIndex;
    private float mouseClickThresholdTime;
    [SerializeField] float maxMouseClickThresholdTime=0.4f;
    public LayerMask enemyLayer;

    //[SerializeField] float meleeAttackCycleTime = 0.5f;
    //[SerializeField] float shootCycleTime = 0.5f;
    //float shootTime = 0, attackTime;
    [SerializeField] Joystick joyStick;
    [SerializeField] ObjectPool bulletPool;
    //[SerializeField] VisualEffect slashEffect;
    [SerializeField] ParticleSystem slashEffect;
    [SerializeField] VisualEffect[] attackVFXs;

    void Start()
    {
        _body = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        _groundChecker = transform.GetChild(0);
        attackIndex = 0;
    }
    void Update()
    {
        _isGrounded = Physics.CheckSphere(_groundChecker.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);
        _inputs = Vector3.zero;
        float h = Input.GetAxis("Horizontal");/* + joyStick.Horizontal;*/
        float v = Input.GetAxis("Vertical"); /*+ joyStick.Vertical;*/
        m_CamForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        _inputs = v * m_CamForward + h * Camera.main.transform.right;
        _inputs = Vector3.ClampMagnitude(_inputs, 1);

        if (_inputs != Vector3.zero) _rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_inputs), Time.deltaTime * RotateSpeed);
        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded) _body.AddForce(Vector3.up * Mathf.Sqrt(JumpHeight * -2f * Physics.gravity.y), ForceMode.VelocityChange);
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Vector3 dashVelocity = Vector3.Scale(transform.forward, DashDistance * new Vector3((Mathf.Log(1f / (Time.deltaTime * _body.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * _body.drag + 1)) / -Time.deltaTime)));
            //_body.AddForce(dashVelocity, ForceMode.VelocityChange);
            _body.MovePosition(_body.position + _inputs.normalized * 3);
        }
        transform.rotation = _rotation;
        Vector3 modMove = transform.InverseTransformDirection(_inputs);
        GetComponent<Animator>().SetFloat("Forward", modMove.z);

        if (mouseClickThresholdTime <= 3) mouseClickThresholdTime += Time.deltaTime;
        //if (shootTime <= shootCycleTime) shootTime += Time.deltaTime;
        //if (attackTime <= meleeAttackCycleTime) attackTime += Time.deltaTime;
        //if (Input.GetMouseButton(0)) Attack();
        if (Input.GetMouseButtonDown(0) && mouseClickThresholdTime > maxMouseClickThresholdTime)
        {
            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Locomotion"))
            {
                // attackIndex = (attackIndex<3)?attackIndex+1:0;
                attackIndex = 0;
                GetComponent<Animator>().SetTrigger("Attack");
                mouseClickThresholdTime = 0;
            }
            else if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("MeleeAttack"))
            {
                int currentFrame = ((int)(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime * (30))) % 30;
                //if (currentFrame > 10)
                {
                    attackIndex = (attackIndex < 5) ? attackIndex + 1 : 0;
                    GetComponent<Animator>().SetTrigger("Attack");
                    mouseClickThresholdTime = 0;
                }
            }
        }
        //GetComponent<Animator>().SetFloat("AttackIndex", attackIndex);
        GetComponent<Animator>().SetFloat("AttackIndex", attackIndex, 0.05f, Time.deltaTime);
    }
    void MeleeHitboxCheck()
    {
        //Collider[] colliders = Physics.OverlapSphere(_body.position + _body.transform.forward * 2, 2, enemyLayer);
        //for (int i = 0; i < colliders.Length; i++)
        //{
        //    colliders[i].gameObject.SendMessage("TakeDamage");
        //}
        //Debug.Log(colliders.Length);
        FindObjectOfType<HitBox>().TargetDamageCheck();
        MeleeVisual();
        Instantiate(slashEffect, transform.position + Vector3.up, transform.rotation);
        for (int i = 0; i < attackVFXs.Length; i++)
        {
            attackVFXs[i].SetFloat("Y Angle", transform.eulerAngles.y);
            attackVFXs[i].Reinit();
            attackVFXs[i].SendEvent("Attack");
        }
    }
    void FixedUpdate()
    {
        //Vector3 v = _inputs * Speed;
        //v.y = _body.velocity.y;
        //_body.velocity = v;
        //if (_inputs != Vector3.zero)            _body.MovePosition(_body.position + _inputs * Speed * Time.fixedDeltaTime);
    }
    private void OnAnimatorMove()
    {
        //// taking current position of root and changing it by movement vector multiplied by deltaPosition which is motion applied to this frame
        //if (_inputs != Vector3.zero) _body.MovePosition(_body.position + _inputs * GetComponent<Animator>().deltaPosition.magnitude);
        //else _body.MovePosition(_body.position + GetComponent<Animator>().deltaPosition);
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Locomotion")) _body.MovePosition(_body.position + _inputs * Speed * Time.fixedDeltaTime);
        _body.MoveRotation(_rotation); //basically changing rotation

        Vector3 v = (_animator.deltaPosition * Speed) / Time.deltaTime;
        if (v.magnitude > 80) v /= 10;
        v.y = _body.velocity.y;
        _body.velocity = v;
    }
  
    //public void Shoot()
    //{
    //    if (shootTime > shootCycleTime)
    //    {
    //        shootTime = 0;
    //        bulletPool.Spawn(transform.position + new Vector3(0, 1, 0), transform.rotation);
    //    }
    //}
    public void MeleeVisual()
    {
        slashEffect.Stop();
        slashEffect.Play();
    }
}
