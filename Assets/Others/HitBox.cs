using Actors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class HitBox : MonoBehaviour
{
    public LayerMask enemyLayer;
    public List<GameObject> targetsInRange = new List<GameObject>();
    [SerializeField] VisualEffect hitEffect;
    //private void OnTriggerEnter(Collider other)
    //{
    //    if ((enemyLayer.value & (1 << other.transform.gameObject.layer)) > 0) other.SendMessage("TakeDamage");
    //}
    public void OnTriggerEnter(Collider other)
    {
        if ((enemyLayer.value & (1 << other.transform.gameObject.layer)) > 0) targetsInRange.Add(other.transform.gameObject);
        //if (other.gameObject.layer == enemyLayer && !targetsInRange.Contains(other.gameObject)) targetsInRange.Add(other.transform.gameObject);
    }
    void OnTriggerExit(Collider other)
    {
        if (targetsInRange.Count > 0) targetsInRange.Remove(other.gameObject);
    }
    public void TargetDamageCheck()
    {
        if (targetsInRange.Count <= 0) return;

        //if (GetComponent<TimeManipulator>() != null) GetComponent<TimeManipulator>().SlowTime();
        EZCameraShake.CameraShaker.Instance.ShakeOnce(3f, 3f, 0.1f, 0.2f);

        foreach (GameObject obj in targetsInRange)
        {
            Instantiate(hitEffect,obj.transform).Play();
            if (obj.GetComponent<DamageTest>() != null)  obj.GetComponent<DamageTest>().TakeDamage(1);
            if (obj.GetComponent<CharacterState>() != null) obj.GetComponent<CharacterState>().ReceiveMeleeDamage(5);
        }
        //Debug.Log(targetsInRange.Count);
    }
}
