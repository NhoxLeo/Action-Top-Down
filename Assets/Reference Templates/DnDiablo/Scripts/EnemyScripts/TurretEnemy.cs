﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemy : Enemy
{
    [SerializeField] private float maxAttackrange;

    protected override void Start()
    {
        base.Start();
        attackDistance = maxAttackrange;
        agent.enabled = false;
    }
    protected override void StateHandler()
    {
        timeAfterAttack -= Time.deltaTime;
        if (timeAfterAttack <= 0) //after an attack, do nothing untill we should act again
        {
            if (attackDistance >= distanceToPlayer) //within range,attack
            {
                transform.LookAt(target);
                timeBeforeAttack -= Time.deltaTime;//Determine if we have been in range for long enough
                if (timeBeforeAttack <= 0) UseSkill(skillsToUse[currentSkillIndex]);
            }
            else timeBeforeAttack = myStats.timeBeforeAttack; // not within range, reset timeBeforeAttack timer
        }
    }
    protected override void Movehandler() { }
    protected override void UseSkill(Skill skill)
    {
        base.UseSkill(skill);
        attackDistance = maxAttackrange;
    }
}
