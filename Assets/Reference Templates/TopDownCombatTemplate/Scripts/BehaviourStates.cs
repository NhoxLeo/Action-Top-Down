﻿using UnityEngine;
using System.Linq;
using Spells;
using Utils;
using Utils.FSM;

namespace AI
{
    public enum AIState
    {
        Wandering,
        AggroMove,
        SpellIntention,
        MovingToSpellRange,
        PerformingCast,
        WaitingAfterCast,
        StartingMeleeAttack,
        WaitingMeleeAttackAnimation,
        EndingMeleeAttack
    }
    /// <summary>
    /// State behaviour that chooses the spell to cast in the next state
    /// </summary>
    class SpellCastIntention : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _nextState;
        private readonly AIState _fallbackState;

        public SpellCastIntention(AIAgent agent, AIState next, AIState fallback)
        {
            _agent = agent;
            _nextState = next;
            _fallbackState = fallback;
        }
        public void StateStarted() { }
        public AIState? StateUpdate()
        {
            if (!_agent.IsAlive())
                return _fallbackState;

            if (!_agent.HasTarget())
                return _fallbackState;

            if (!_agent.IsBetweenFearAndAggro())
                return _fallbackState;

            if (!_agent.CanCast())
                return _fallbackState;

            // If no intentions to choose from -> fallback
            if (_agent.Config.AI.SlotConfig == null || _agent.Config.AI.SlotConfig.Length == 0)
                return _fallbackState;

            // Select a slot to cast that is ready;
            var readySlots = _agent.Config.AI.SlotConfig.Where(s =>
            {
                var slotState = _agent.SpellBook.GetSpellSlotState(s.Slot);
                return slotState.State == SpellbookState.SlotState.Ready;
            }).ToList();

            // Find and randomly choose an active slot
            if (readySlots.Count >= 0)
            {
                var intention = RandomUtils.Choice(readySlots, s => s.Weight);
                _agent.IntendedSpell = intention;
                return _nextState;
            }

            // No slots to cast
            _agent.Logger.Log("No active slots to cast");
            return _fallbackState;
        }
        public void StateEnded() { }
    }
    /// <summary>
    /// This state performs spell casting. Including channeling
    /// It uses "spell intention" from memory. It should be set from another state
    /// </summary>
    class CastIntendedSpell : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _nextState;
        private readonly AIState _fallbackState;
        private bool _isCasting;
        private readonly TargetLocationProvider _targetProxy;

        public CastIntendedSpell(AIAgent agent, AIState next, AIState fallback)
        {
            _agent = agent;
            _nextState = next;
            _fallbackState = fallback;
            _targetProxy = new TargetLocationProvider
            {
                IsValid = false
            };
        }
        public void StateStarted()
        {
            _isCasting = false;
            _targetProxy.IsValid = false;
        }
        public AIState? StateUpdate()
        {
            if (!_agent.IsAlive())
            {
                AbortIfCasting();
                return _fallbackState;
            }

            if (!_agent.HasTarget())
            {
                AbortIfCasting();
                return _fallbackState;
            }

            if (!_agent.IsBetweenFearAndAggro())
            {
                AbortIfCasting();
                return _fallbackState;
            }

            if (_agent.IntendedSpell == null)
            {
                AbortIfCasting();
                return _fallbackState;
            }

            var slotState = _agent.SpellBook.GetSpellSlotState(_agent.IntendedSpell.Slot);
            if (slotState.State == SpellbookState.SlotState.Ready)
            {
                if (_isCasting)
                {
                    // If we are casting and ended up in ready state
                    // then it means that the cast is completed
                    _agent.LastSpellCastTime = Time.time;
                    return _nextState;
                }

                // If AI is not casting and the slot is ready
                // Cast the spell
                _agent.Movement.ControlLookAt(_agent.ActiveTarget.transform.position);
                _agent.Movement.ControlStop();

                if (_agent.SpellBook.TryFireSpellToTarget(_agent.IntendedSpell.Slot,
                    GetTarget(slotState.Spell.TargetType)))
                {
                    _isCasting = true;
                    return null;
                }

                // Casting failed, fallback
                return _fallbackState;
            }
            else if (slotState.State == SpellbookState.SlotState.Firing ||
                    slotState.State == SpellbookState.SlotState.Preparing)
            {
                // Update target while casting
                _targetProxy.Location = _agent.ActiveTarget.transform.position;

                // We are casting, we cool
                return null;
            }

            // Failed to cast
            return _fallbackState;
        }
        public void StateEnded()
        {
            _targetProxy.IsValid = false;
        }
        private void AbortIfCasting()
        {
            _targetProxy.IsValid = false;

            if (_agent.IntendedSpell == null)
                return;

            var slotState = _agent.SpellBook.GetSpellSlotState(_agent.IntendedSpell.Slot);
            slotState.SpellHandler?.Abort();
        }
        private Target GetTarget(TargetType type)
        {
            switch (type)
            {
                case TargetType.None:
                    return Target.None;
                case TargetType.Location:
                    var t = _agent.ActiveTarget.transform;
                    return new Target(t.position, t.forward);
                case TargetType.Transform:
                    return new Target(_agent.ActiveTarget.transform);
                case TargetType.LocationProvider:
                    _targetProxy.IsValid = true;
                    _targetProxy.Location = _agent.ActiveTarget.transform.position;
                    return new Target(_targetProxy);
                case TargetType.Character:
                    return new Target(_agent.ActiveTarget);
                default:
                    return Target.None;
            }
        }
    }
    class MoveToTargetRangeState : IStateBehaviour<AIState>
    {
        public enum MoveMode
        {
            Inside,
            Outside,
            Edge
        }
        private readonly float _targetRange;
        private readonly float _edgeTolerance;
        private readonly AIState _inRangeState;
        private readonly AIState _outOfAggroRangeState;
        private readonly AIAgent _agent;
        private readonly MoveMode _moveMode;

        public MoveToTargetRangeState(
            AIAgent agent,
            float targetRange,
            AIState inRangeState,
            AIState outOfAggroRangeState,
            MoveMode moveMode = MoveMode.Inside,
            float edgeTolerance = 1f)
        {
            _edgeTolerance = edgeTolerance;
            _moveMode = moveMode;
            _agent = agent;
            _targetRange = targetRange;
            _inRangeState = inRangeState;
            _outOfAggroRangeState = outOfAggroRangeState;
        }
        public void StateStarted() { }
        public AIState? StateUpdate()
        {
            if (!_agent.IsAlive())
                return _outOfAggroRangeState;

            if (!_agent.HasTarget())
                return _outOfAggroRangeState;

            // Distance and direction
            var direction = _agent.ActiveTarget.transform.position - _agent.Transform.position;
            var distance = (direction).magnitude;
            direction = direction.normalized;

            // We are too far from target
            if (distance > _agent.Config.AI.AggroRange)
                return _outOfAggroRangeState;

            Vector3 targetPosition;
            switch (_moveMode)
            {
                case MoveMode.Inside:
                    if (distance < _targetRange && distance < _agent.Config.AI.AggroRange)
                        return _inRangeState;
                    targetPosition = _agent.ActiveTarget.transform.position - direction * (_targetRange - 0.1f);
                    break;
                case MoveMode.Outside:
                    if (distance > _targetRange && distance < _agent.Config.AI.AggroRange)
                        return _inRangeState;
                    targetPosition = _agent.ActiveTarget.transform.position - direction * (_targetRange + 0.1f);
                    break;
                case MoveMode.Edge:
                    if (Mathf.Abs(distance - _targetRange) < _edgeTolerance && distance < _agent.Config.AI.AggroRange)
                        return _inRangeState;
                    targetPosition = _agent.ActiveTarget.transform.position - direction * _targetRange;
                    break;
                default:
                    targetPosition = _agent.ActiveTarget.transform.position - direction * _targetRange;
                    break;
            }

            // Do actual movement
            _agent.Movement.ControlSetDestination(targetPosition);
            _agent.Movement.ControlLookAt(targetPosition);

            // Return same state
            return null;
        }
        public void StateEnded() { }
    }
    class WanderState : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _aggroState;

        public WanderState(AIAgent agent, AIState aggroState)
        {
            _agent = agent;
            _aggroState = aggroState;
        }
        public void StateStarted()
        {
        }
        public AIState? StateUpdate()
        {
            if (_agent.ActiveTarget == null || !_agent.ActiveTarget.IsAlive)
            {
                _agent.ActiveTarget = null;
                // TODO: add target searching
                return null;
            }

            var distance = (_agent.ActiveTarget.transform.position - _agent.Transform.position).magnitude;
            if (distance < _agent.Config.AI.AggroRange)
                return _aggroState;

            // TODO: Add random movement maybe ?
            // Do nothing, we are wandering and not in aggro
            return null;
        }
        public void StateEnded() { }
    }
    class BufferTryBuff : IStateBehaviour<AIState>
    {
        private AIAgent _agent;

        public BufferTryBuff(AIAgent agent)
        {
            _agent = agent;
        }
        public void StateStarted() { }
        public AIState? StateUpdate()
        {
            /*
             *
             * yield return null;
        while (_characterState.IsAlive && _player.IsAlive)
        {
            var len = _player.transform.position - transform.position;
            var distance = len.magnitude;
            _bufferCheck += Time.deltaTime;

            if ((_buffTarget == null || !_buffTarget.IsAlive) && _bufferCheck>1f)
            {
                _logger.Log("Found target");
                _bufferCheck = 0;
                var allies = GameObject.FindGameObjectsWithTag(Common.Tags.Enemy).Select(o => o.GetComponent<CharacterState>()).ToArray();
                if (allies.Length > 0)
                    _buffTarget = RandomUtils.Choice(allies);                
            }

            if (_buffTarget == null || distance < _fearRange || !_buffTarget.IsAlive)
            {
                StartCoroutine(FearOrSpellState(BufferWanderState(), len, distance));
                break;
            }
            else
            {
                _movement.Stop();
                var buffTarget = _buffTarget.GetComponent<CharacterState>();

                buffTarget.ApplyBuff(_useBuff, buffTarget, null, 1);
                _animationController.PlayCastAnimation();
            }
            yield return null;
        }
             */

            return null;
        }
        public void StateEnded() { }
    }
    class WaitingInRangeState : IStateBehaviour<AIState>
    {
        private readonly float _waitTime;
        private readonly float _keepRange;
        private readonly AIState _targetOutOfRange;
        private readonly AIState _finished;
        private readonly AIAgent _agent;
        private readonly float _tolerance;
        private float _timePassed;

        public WaitingInRangeState(AIAgent agent,
            float time,
            float keepRange,
            AIState finishedWaiting,
            AIState targetOutOfRange,
            float tolerance = 1f)
        {
            _agent = agent;
            _finished = finishedWaiting;
            _targetOutOfRange = targetOutOfRange;
            _keepRange = keepRange;
            _waitTime = time;
            _tolerance = tolerance;
        }
        public void StateStarted()
        {
            _timePassed = 0;
        }
        public AIState? StateUpdate()
        {
            _timePassed += Time.deltaTime;
            if (_timePassed > _waitTime)
                return _finished;

            if (_agent.ActiveTarget != null && _agent.ActiveTarget.IsAlive)
            {
                _agent.Movement.ControlLookAt(_agent.ActiveTarget.transform.position);

                var distance = (_agent.ActiveTarget.transform.position - _agent.Transform.position).magnitude;
                if (Mathf.Abs(distance - _keepRange) > _tolerance)
                    return _targetOutOfRange;
            }

            return null;
        }
        public void StateEnded() { }
    }
    class WaitAfterIntendedSpellCast : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _nextState;
        private readonly AIState _fallbackState;
        private float _waitingTime;

        public WaitAfterIntendedSpellCast(AIAgent agent, AIState next, AIState fallback)
        {
            _agent = agent;
            _nextState = next;
            _fallbackState = fallback;
        }
        public void StateStarted()
        {
            _waitingTime = 0f;
        }
        public AIState? StateUpdate()
        {
            if (_agent.IntendedSpell == null)
                return _fallbackState;

            _waitingTime += Time.deltaTime;
            if (_waitingTime > _agent.IntendedSpell.WaitAfterCast)
            {
                _waitingTime = 0;
                return _nextState;
            }

            return null;
        }
        public void StateEnded() { }
    }
    class StartMeleeAttack : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _nextState;
        private readonly AIState _fallbackState;

        public StartMeleeAttack(AIAgent agent, AIState nextState, AIState fallbackState)
        {
            _agent = agent;
            _nextState = nextState;
            _fallbackState = fallbackState;
        }
        public void StateStarted() { }
        public AIState? StateUpdate()
        {
            if (_agent.ActiveTarget == null || !_agent.ActiveTarget.IsAlive)
                return _fallbackState;

            if (Time.time > _agent.Config.AI.MeleeAttackCooldown + _agent.LastAttackTime)
            {
                _agent.Movement.ControlStop();
                _agent.Movement.ControlLookAt(_agent.ActiveTarget.transform.position);
                _agent.AnimationController.PlayAttackAnimation();
                return _nextState;
            }

            return _fallbackState;
        }
        public void StateEnded() { }
    }
    class DealMeleeDamage : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _nextState;
        private readonly AIState _fallbackState;

        public DealMeleeDamage(AIAgent agent, AIState nextState, AIState fallback)
        {
            _agent = agent;
            _nextState = nextState;
            _fallbackState = fallback;
        }
        public void StateStarted() { }
        public AIState? StateUpdate()
        {
            if (_agent.ActiveTarget == null || !_agent.ActiveTarget.IsAlive)
                return _fallbackState;

            if (Vector3.Distance(_agent.ActiveTarget.transform.position, _agent.Transform.position) >
                _agent.Config.AI.MaxMeleeRange)
                return _fallbackState;

            _agent.LastAttackTime = Time.time;
            _agent.ActiveTarget.ReceiveMeleeDamage(_agent.CharacterState.Damage);
            if (_agent.Config.AI.MeleeAttackBuff != null)
                _agent.ActiveTarget.ApplyBuff(_agent.Config.AI.MeleeAttackBuff, _agent.CharacterState, 1 + _agent.CharacterState.AdditionSpellStacks);

            return _nextState;
        }
        public void StateEnded() { }
    }
    class AdjustPositionToCastIntendedSpell : IStateBehaviour<AIState>
    {
        private readonly AIAgent _agent;
        private readonly AIState _nextState;
        private readonly AIState _fallbackState;
        private float _desiredRange;

        public AdjustPositionToCastIntendedSpell(AIAgent agent, AIState next, AIState fallback)
        {
            _agent = agent;
            _nextState = next;
            _fallbackState = fallback;
        }
        public void StateStarted()
        {
            if (_agent?.IntendedSpell != null)
            {
                var slotState = _agent.SpellBook.GetSpellSlotState(_agent.IntendedSpell.Slot);
                _desiredRange = slotState.Spell.MaxRange.GetValue(slotState.NumStacks);
            }
        }
        public AIState? StateUpdate()
        {
            if (!_agent.IsAlive())
                return _fallbackState;

            if (!_agent.HasTarget())
                return _fallbackState;

            if (!_agent.IsBetweenFearAndAggro())
                return _fallbackState;

            var distance = _agent.GetLineDistanceToTarget();
            if (distance < _desiredRange)
                return _nextState;

            var direction = (_agent.ActiveTarget.transform.position - _agent.Transform.position).normalized;

            var moveTo = _agent.ActiveTarget.transform.position - direction * (_desiredRange - 0.1f);
            _agent.Movement.ControlSetDestination(moveTo);
            _agent.Movement.ControlLookAt(moveTo);

            return null;
        }
        public void StateEnded() { }
    }
}