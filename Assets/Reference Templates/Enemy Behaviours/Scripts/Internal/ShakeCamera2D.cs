namespace EnemyBehaviours.Internal
{
    using UnityEngine;

    [AddComponentMenu("Enemy Behaviours/Internal/ShakeCamera2D", 5)]
    public class ShakeCamera2D : MonoBehaviour
    {
        public float shakeMagnitude = 0.7f;
        public float dampingSpeed = 1;

        void LateUpdate()
        {
            transform.localPosition += Random.insideUnitSphere * shakeMagnitude;
        }
    }
}