namespace EnemyBehaviours.Internal
{
    using UnityEngine;

    [AddComponentMenu("Enemy Behaviours/Internal/LifeTime", 7)]
    public class LifeTime : MonoBehaviour
    {
        public float time = 3f;
        
        void OnEnable()
        {
            Invoke("Die", time);
        }

        void Die()
        {
            Destroy(gameObject);
        }
    }
}