namespace EnemyBehaviours.Movement
{
    using UnityEngine;
    using UnityEngine.Events;
    using EnemyBehaviours.Internal;

    /// <summary>
    /// Jumps.
    /// </summary>
    [AddComponentMenu("Enemy Behaviours/Movement/Jumper", 6)]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class Jumper: MonoBehaviour
    {
        public Vector3 jumpSpeed = Vector3.one;
        public bool jumpOnEnable = true;

        [Header("Autochange jump direction (to player tagged position).")]
        public bool jumpToPlayer = false;

        [Header("Called when ground is achieved.")]
        public UnityEvent OnGround;

        private bool inJump = false;
        private Rigidbody rb2D;
        private Collider coll2D;
        private GroundCheck groundCheck;
        private GameObject groundCheckGO;

        void OnEnable()
        {
            rb2D = GetComponent<Rigidbody>();
            coll2D = GetComponent<Collider>();
            
            if (groundCheckGO != null)
                Destroy(groundCheckGO);
            var yHalfExtents = coll2D.bounds.extents.y;
            var yCenter = coll2D.bounds.center.y;
            float yLower = transform.position.y + (yCenter - yHalfExtents);
            var go = new GameObject();
            groundCheckGO = Instantiate(go, new Vector3(transform.position.x, yLower, transform.position.z), Quaternion.identity);
            Destroy(go);
            groundCheckGO.name = "GroundCheck";
            groundCheckGO.AddComponent<BoxCollider2D>().isTrigger = true;
            groundCheck = groundCheckGO.AddComponent<GroundCheck>();
            groundCheckGO.transform.SetParent(transform);
            groundCheckGO.GetComponent<BoxCollider2D>().size = new Vector3(0.2f, 0.2f);

            if (jumpOnEnable)
                Jump();
        }


        /// <summary>
        /// Jumping method. Can be called from Inspector.
        /// </summary>
        public void Jump()
        {
            if (!rb2D.useGravity)
                rb2D.useGravity = true;
            rb2D.freezeRotation = true;
            if (jumpToPlayer)
            {
                var sign = (int)Mathf.Sign(GameObject.FindWithTag("Player").transform.position.x - transform.position.x);
                jumpSpeed.x = Mathf.Abs(jumpSpeed.x) * 
                    sign;
                Flip(-sign);
            }
            rb2D.velocity += jumpSpeed;
            inJump = true;
        }

        /// <summary>
        /// Stops body (to avoid skidding). Can be called from Inspector.
        /// </summary>
        public void Stop()
        {
            rb2D.velocity = Vector3.zero;
        }

        void FixedUpdate()
        {
            if (!(coll2D is BoxCollider || coll2D is CapsuleCollider))
            {
                Debug.LogWarning(name + ": Jumper: BoxCollider and CapsuleCollider are supported only. Fix this and re-enable script.");
                return;
            }

            if (inJump && groundCheck.IsGrounded())
            {
                inJump = false;
                Stop();
                OnGround.Invoke();
            } 
        }

        /// <summary>
        /// Flips object.
        /// </summary>
        void Flip(int dir = 1)
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= Mathf.Sign(theScale.x);
            if (Mathf.Sign(dir) == -1)
               theScale.x *= -1; 
            transform.localScale = theScale;
        }
    } 
}