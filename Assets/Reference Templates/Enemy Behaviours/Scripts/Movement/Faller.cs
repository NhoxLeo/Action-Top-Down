namespace EnemyBehaviours.Movement
{
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Falls down.
    /// </summary>
    [AddComponentMenu("Enemy Behaviours/Movement/Faller", 4)]
    [RequireComponent(typeof(Rigidbody))]
    public class Faller: MonoBehaviour
    {
        [RangeAttribute(0f, 10f)]
        [Header("Speed of falling.")]
        public float speed = 1;

        [Header("Event called when surface is achieved.")]
        public UnityEvent OnAchieveSurface;
        
        private Rigidbody rb2D;

        /// <summary>
        /// Resets values and repeat movement.
        /// </summary>
        void Init()
        {
            if (GetComponent<Collider>() == null)
            Debug.LogWarning(name + ": Faller: There is no 2D collider.");
            rb2D = GetComponent<Rigidbody>();
            rb2D.useGravity = false;
            rb2D.velocity = Vector3.down * speed;
        }

        void OnEnable()
        {
            // initialize
            Init();
        }

        void Update()
        {
            rb2D.velocity = Vector3.down * speed;
        }

        void OnCollisionEnter(Collision other)
        {
            // check if collision is not with ceiling
            if (other.collider.transform.position.y < transform.position.y)
                OnAchieveSurface.Invoke();
        }

        void OnTriggerEnter(Collider other)
        {
            // check if collision is not with ceiling
            if (other.transform.position.y < transform.position.y)
                OnAchieveSurface.Invoke();
        }
    }
}